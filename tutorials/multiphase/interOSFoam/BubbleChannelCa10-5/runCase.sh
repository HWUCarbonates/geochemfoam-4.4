#!/bin/bash

set -e

decomposePar
mpiexec -np 12 interOSFoam  -parallel > log.out
reconstructPar
rm -rf proc*
./writeStep.sh
rm log.out
